﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using LoggedIn.Controllers;
using Logged_In.Models;

namespace Logged_In.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Received()
        {
            ViewBag.User = HttpContext.Session.GetString(UserController.ActiveUser);

            return View();
        }
        
        public IActionResult Login()
        {
            return View();
        }
        
        public IActionResult Completed()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
