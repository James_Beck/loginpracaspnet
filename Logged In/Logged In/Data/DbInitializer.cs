﻿using System;
using System.Linq;
using LoggedIn.Models;

namespace LoggedIn.Data
{
    public class DbInitializer
    {
        public static void Initialize(UserContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
